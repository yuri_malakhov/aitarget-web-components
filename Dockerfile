FROM node:8-alpine

RUN npm install http-server -g

WORKDIR /app

COPY ./package* /app/

RUN npm install

COPY . /app

RUN npm run build

RUN rm -rf node_modules

VOLUME ["/app"]

ENTRYPOINT ["sh", "-c"]

CMD ["http-server -p 4300 --cors --silent -d=false ./dist"]

EXPOSE 4300 8080
