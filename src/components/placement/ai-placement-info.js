class AiPlacementInfo extends HTMLElement {
  constructor () {
    super();
    // Define shadow DOM
    this._root = this.attachShadow({ mode: "open" });

    // Main template
    const $template = document.createElement("template");
    $template.innerHTML = `
<slot name="title"></slot>
<slot name="content"></slot>
<style>
:host {
  display: block;
  padding: 10px 5px;
}
[name=title]::slotted(*) {
  color: #90949c;
  font-weight: bold;
  font-size: 0.8em;
  text-decoration: none;
  text-transform: uppercase;
}
</style>
`;
    this.$template = document.importNode($template.content, true);
  }

  connectedCallback () {
    this._root.appendChild(this.$template);
  }
}

window.customElements.define('ai-placement-info', AiPlacementInfo);
