class AiPlacementDevices extends HTMLElement {
  constructor () {
    super();
    // Define shadow DOM
    this._root = this.attachShadow({ mode: "open" });

    // Main template
    const $template = document.createElement("template");
    $template.innerHTML = `<div class="device mobile"><ai-icon-mobile></ai-icon-mobile><span class="name"></span></div>
                           <div class="device desktop"><ai-icon-desktop></ai-icon-desktop><span class="name"></span></div>
                           <style>.disabled {opacity: 0.5;} .name {display: inline-block; margin-left: 5px;}</style>`;
    this.$template = document.importNode($template.content, true);
  }

  connectedCallback () {
    ['mobile', 'desktop'].forEach((name) => {
      const $device = this.$template.querySelector(`.${name}`);
      const $name = $device.querySelector(`.name`);
      $name.innerHTML = `${name.slice(0, 1)
                               .toUpperCase() + name.slice(1)}`;
      if (this.dataset.devices.indexOf(name) > -1) {
        $name.innerHTML = $name.innerHTML + ` Enabled`;
      } else {
        $device.classList.add('disabled');
        $name.innerHTML = $name.innerHTML + ` Disabled`;
      }
    });

    this._root.appendChild(this.$template);
  }
}

window.customElements.define('ai-placement-devices', AiPlacementDevices);
