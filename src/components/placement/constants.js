export const POSITIONS = {
  FACEBOOK:         'facebook_positions',
  INSTAGRAM:        'instagram_positions',
  AUDIENCE_NETWORK: 'audience_network_positions',
  MESSENGER:        'messenger_positions'
};

export const KEYS = {
  FEED:               'feed',
  RIGHT_HAND_COLUMN:  'right_hand_column',
  INSTANT_ARTICLE:    'instant_article',
  INSTREAM_VIDEO:     'instream_video',
  SUGGESTED_VIDEO:    'suggested_video',
  STREAM:             'stream',
  STORY:              'story',
  CLASSIC:            'classic',
  REWARDED_VIDEO:     'rewarded_video',
  MESSENGER_HOME:     'messenger_home',
  SPONSORED_MESSAGES: 'sponsored_messages'
};

export const OBJECTIVES = {
  BRAND_AWARENESS:       'BRAND_AWARENESS',
  APP_INSTALLS:          'APP_INSTALLS',
  CONVERSIONS:           'CONVERSIONS',
  EVENT_RESPONSES:       'EVENT_RESPONSES',
  LEAD_GENERATION:       'LEAD_GENERATION',
  LINK_CLICKS:           'LINK_CLICKS',
  OFFER_CLAIMS:          'OFFER_CLAIMS',
  PAGE_LIKES:            'PAGE_LIKES',
  POST_ENGAGEMENT:       'POST_ENGAGEMENT',
  VIDEO_VIEWS:           'VIDEO_VIEWS',
  REACH:                 'REACH',
  PRODUCT_CATALOG_SALES: 'PRODUCT_CATALOG_SALES',
  STORE_VISITS:          'STORE_VISITS'
};

export const CREATIVES = {
  ALL:                                 'all',
  APP_ADS_DESKTOP:                     'app_ads_desktop',
  APP_ADS_PHOTO_VIDEO_MOBILE:          'app_ads_photo_video_mobile',
  LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO: 'link_ads_page_connected_photo_video',
  LINK_ADS_PAGE_DISCONNECTED:          'link_ads_page_disconnected',
  EVENT_ADS:                           'event_ads',
  PAGE_POST_ADS:                       'page_post_ads',
  OFFER_ADS:                           'offer_ads',
  VIDEO_CREATIVES:                     'video_creatives',
  PAGE_POST_ADS_PHOTO_VIDEO:           'page_post_ads_photo_video',
  VIDEO_ADS:                           'video_ads',
  REACH_ADS:                           'reach_ads',
  DYNAMIC_ADS:                         'dynamic_ads',
  STORE_VISIT_ADS:                     'store_visit_ads'
};

export const DEVICES = {
  ALL:     'mobile,desktop',
  MOBILE:  'mobile',
  DESKTOP: 'desktop',
};
