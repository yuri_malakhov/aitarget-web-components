export function restrictionKey (objective, creative, device) {
  return `${objective}.${creative}.${device}`;
}
