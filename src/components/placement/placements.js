import { CREATIVES, DEVICES, KEYS, OBJECTIVES, POSITIONS } from './constants';
import { restrictionKey } from './keys';

export function placements () {
  /*  console.log(`objective, creative: `, objective, creative);

    function inaligiblePlacement () {
      return {
        title:    `INELIGIBLE PLACEMENT`,
        content:  `This placement isn't available with the selected objective.`,
        content2: `This platform isn't available with the desktop device type.`
      };
    }*/

  return [
    {
      key:                 POSITIONS.FACEBOOK,
      value:               'Facebook',
      allowedInObjectives: [
        OBJECTIVES.BRAND_AWARENESS,
        OBJECTIVES.APP_INSTALLS,
        OBJECTIVES.APP_INSTALLS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.EVENT_RESPONSES,
        OBJECTIVES.EVENT_RESPONSES,
        OBJECTIVES.LEAD_GENERATION,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.OFFER_CLAIMS,
        OBJECTIVES.PAGE_LIKES,
        OBJECTIVES.POST_ENGAGEMENT,
        OBJECTIVES.POST_ENGAGEMENT,
        OBJECTIVES.VIDEO_VIEWS,
        OBJECTIVES.REACH,
        OBJECTIVES.PRODUCT_CATALOG_SALES,
        OBJECTIVES.STORE_VISITS
      ],
      allowedInCreatives:  [
        CREATIVES.ALL,
        CREATIVES.APP_ADS_DESKTOP,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.LINK_ADS_PAGE_DISCONNECTED,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.EVENT_ADS,
        CREATIVES.PAGE_POST_ADS,
        CREATIVES.PAGE_POST_ADS,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.LINK_ADS_PAGE_DISCONNECTED,
        CREATIVES.APP_ADS_DESKTOP,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.OFFER_ADS,
        CREATIVES.VIDEO_CREATIVES,
        CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
        CREATIVES.PAGE_POST_ADS,
        CREATIVES.VIDEO_ADS,
        CREATIVES.REACH_ADS,
        CREATIVES.DYNAMIC_ADS,
        CREATIVES.STORE_VISIT_ADS
      ],
      allowedInDevices:    [DEVICES.ALL, DEVICES.MOBILE, DEVICES.DESKTOP],
      allowedIn:           [
        restrictionKey(OBJECTIVES.BRAND_AWARENESS, CREATIVES.ALL, DEVICES.ALL),
        restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_DISCONNECTED, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.EVENT_ADS, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LEAD_GENERATION, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_DISCONNECTED, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.PAGE_LIKES, CREATIVES.VIDEO_CREATIVES, DEVICES.ALL),
        restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.STORE_VISITS, CREATIVES.STORE_VISIT_ADS, DEVICES.ALL),
      ],
      info:                [
        {
          title:   'Placements',
          content: `<p>Facebook - Feeds, Instant Articles, Instream - Videos, Right Column and Suggested Videos</p>`
        },
        {
          title:   'Devices',
          content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
        }
      ],
      children:            [
        {
          key:                 KEYS.FEED,
          value:               'Feed',
          allowedInObjectives: [
            OBJECTIVES.BRAND_AWARENESS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.EVENT_RESPONSES,
            OBJECTIVES.LEAD_GENERATION,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.PAGE_LIKES,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
            OBJECTIVES.STORE_VISITS,
          ],
          allowedInCreatives:  [
            CREATIVES.ALL,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_CREATIVES,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
            CREATIVES.STORE_VISIT_ADS,
          ],
          allowedInDevices:    [DEVICES.ALL, DEVICES.MOBILE, DEVICES.DESKTOP],
          allowedIn:           [
            restrictionKey(OBJECTIVES.BRAND_AWARENESS, CREATIVES.ALL, DEVICES.ALL),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LEAD_GENERATION, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PAGE_LIKES, CREATIVES.VIDEO_CREATIVES, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.STORE_VISITS, CREATIVES.STORE_VISIT_ADS, DEVICES.ALL)
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Facebook - Feeds - News Feed</p>`,
              demo:    `<ai-icon-feed></ai-icon-feed>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
            }
          ]
        },
        {
          key:                 KEYS.INSTANT_ARTICLE,
          value:               'Instant Articles',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.EVENT_RESPONSES,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.PAGE_LIKES,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
            OBJECTIVES.STORE_VISITS,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_CREATIVES,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
            CREATIVES.STORE_VISIT_ADS,
          ],
          allowedInDevices:    [DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PAGE_LIKES, CREATIVES.VIDEO_CREATIVES, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.STORE_VISITS, CREATIVES.STORE_VISIT_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Facebook - Instant Articles - Instant Articles</p>`,
              demo:    `<ai-icon-feed></ai-icon-feed>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
            }
          ]
        },
        {
          key:                 KEYS.INSTREAM_VIDEO,
          value:               'In-Stream Videos',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.EVENT_RESPONSES,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.PAGE_LIKES,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
            OBJECTIVES.STORE_VISITS,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_CREATIVES,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
            CREATIVES.STORE_VISIT_ADS,
          ],
          allowedInDevices:    [DEVICES.ALL, DEVICES.MOBILE, DEVICES.DESKTOP],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PAGE_LIKES, CREATIVES.VIDEO_CREATIVES, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.STORE_VISITS, CREATIVES.STORE_VISIT_ADS, DEVICES.ALL)
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Facebook-In-Stream Videos-In-Stream Videos</p>`,
              demo:    `<ai-icon-feed></ai-icon-feed>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
            }
          ]
        },
        {
          key:                 KEYS.RIGHT_HAND_COLUMN,
          value:               'Right Column',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.EVENT_RESPONSES,
            OBJECTIVES.EVENT_RESPONSES,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.PAGE_LIKES,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
            OBJECTIVES.STORE_VISITS,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.LINK_ADS_PAGE_DISCONNECTED,
            CREATIVES.EVENT_ADS,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.LINK_ADS_PAGE_DISCONNECTED,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_CREATIVES,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
            CREATIVES.STORE_VISIT_ADS,
          ],
          allowedInDevices:    [DEVICES.DESKTOP],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_DISCONNECTED, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.EVENT_ADS, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_DISCONNECTED, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PAGE_LIKES, CREATIVES.VIDEO_CREATIVES, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.STORE_VISITS, CREATIVES.STORE_VISIT_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   `Placements`,
              content: `<p>Facebook-Right Column-Right Column</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.DESKTOP}"></ai-placement-devices>`
            }
          ]
        },
        {
          key:                 KEYS.SUGGESTED_VIDEO,
          value:               'Suggested Videos',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.EVENT_RESPONSES,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.PAGE_LIKES,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
            OBJECTIVES.STORE_VISITS,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_CREATIVES,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
            CREATIVES.STORE_VISIT_ADS,
          ],
          allowedInDevices:    [DEVICES.ALL, DEVICES.MOBILE, DEVICES.DESKTOP],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.EVENT_RESPONSES, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PAGE_LIKES, CREATIVES.VIDEO_CREATIVES, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.STORE_VISITS, CREATIVES.STORE_VISIT_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Facebook-Suggested Videos-Suggested Videos</p>`,
              demo:    `<ai-icon-feed></ai-icon-feed>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
            }
          ]
        }
      ]
    },
    {
      key:                 POSITIONS.INSTAGRAM,
      value:               'Instagram',
      allowedInObjectives: [
        OBJECTIVES.BRAND_AWARENESS,
        OBJECTIVES.APP_INSTALLS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.LEAD_GENERATION,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.POST_ENGAGEMENT,
        OBJECTIVES.VIDEO_VIEWS,
        OBJECTIVES.REACH,
        OBJECTIVES.PRODUCT_CATALOG_SALES,
      ],
      allowedInCreatives:  [
        CREATIVES.ALL,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.PAGE_POST_ADS,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
        CREATIVES.VIDEO_ADS,
        CREATIVES.REACH_ADS,
        CREATIVES.DYNAMIC_ADS,
      ],
      allowedInDevices:    [DEVICES.MOBILE],
      allowedIn:           [
        restrictionKey(OBJECTIVES.BRAND_AWARENESS, CREATIVES.ALL, DEVICES.ALL),
        restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.LEAD_GENERATION, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL)
      ],
      info:                [
        {
          title:   'Placements',
          content: `<p>Instagram-Feed and Stories</p>`
        },
        {
          title:   'Devices',
          content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
        }
      ],
      children:            [
        {
          key:                 KEYS.STREAM,
          value:               'Feed',
          allowedInObjectives: [
            OBJECTIVES.BRAND_AWARENESS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LEAD_GENERATION,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.ALL,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.BRAND_AWARENESS, CREATIVES.ALL, DEVICES.ALL),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LEAD_GENERATION, CREATIVES.PAGE_POST_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Instagram-Feed-Feed</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
            }
          ],
        },
        {
          key:                 KEYS.STORY,
          value:               'Stories',
          allowedInObjectives: [
            OBJECTIVES.BRAND_AWARENESS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.POST_ENGAGEMENT,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.ALL,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.BRAND_AWARENESS, CREATIVES.ALL, DEVICES.ALL),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.POST_ENGAGEMENT, CREATIVES.PAGE_POST_ADS_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Instagram-Stories-Stories</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
            }
          ],
        }
      ]
    },
    {
      key:                 POSITIONS.AUDIENCE_NETWORK,
      value:               'Audience Network',
      allowedInObjectives: [
        OBJECTIVES.APP_INSTALLS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.OFFER_CLAIMS,
        OBJECTIVES.VIDEO_VIEWS,
        OBJECTIVES.REACH,
        OBJECTIVES.PRODUCT_CATALOG_SALES,
      ],
      allowedInCreatives:  [
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.OFFER_ADS,
        CREATIVES.VIDEO_ADS,
        CREATIVES.REACH_ADS,
        CREATIVES.DYNAMIC_ADS,
      ],
      allowedInDevices:    [DEVICES.MOBILE],
      allowedIn:           [
        restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
      ],
      info:                [
        {
          title:   'Placements',
          content: `<p>Audience Network-Native, Banner and Interstitial, In-Stream Videos and Rewarded Videos</p>
                    <p>Extend your reach by showing ads to the same target audience on other mobile apps and websites.</p>`
        },
        {
          title:   'Devices',
          content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
        }
      ],
      children:            [
        {
          key:                 KEYS.CLASSIC,
          value:               'Native, Banner and Interstitial',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Audience Network-Native, Banner and Interstitial-Native, Banner and Interstitial</p>
                        <p>Ads may appear in native, banner and interstitial placements on Audience Network apps and sites. Banners will not be used for the video views objective.</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
            }
          ],
        },
        {
          key:                 KEYS.INSTREAM_VIDEO,
          value:               'In-Stream Videos',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.ALL, DEVICES.DESKTOP, DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Audience Network-In-Stream Videos-In-Stream Videos</p>
                        <p>Your video ad may be shown pre-roll and mid-roll, within videos on Audience Network apps and websites.</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
            }
          ]
        },
        {
          key:                 KEYS.REWARDED_VIDEO,
          value:               'Rewarded Videos',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.OFFER_CLAIMS,
            OBJECTIVES.VIDEO_VIEWS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.OFFER_ADS,
            CREATIVES.VIDEO_ADS,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.OFFER_CLAIMS, CREATIVES.OFFER_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.VIDEO_VIEWS, CREATIVES.VIDEO_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Audience Network-Rewarded Videos-Rewarded Videos</p>
                        <p>Your ad may appear in apps where people choose to watch the entire ad in exchange for in-app rewards.</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
            }
          ]
        }
      ]
    },
    {
      key:                 POSITIONS.MESSENGER,
      value:               'Messenger',
      allowedInObjectives: [
        OBJECTIVES.APP_INSTALLS,
        OBJECTIVES.APP_INSTALLS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.CONVERSIONS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.LINK_CLICKS,
        OBJECTIVES.REACH,
        OBJECTIVES.PRODUCT_CATALOG_SALES,
      ],
      allowedInCreatives:  [
        CREATIVES.APP_ADS_DESKTOP,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
        CREATIVES.APP_ADS_DESKTOP,
        CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
        CREATIVES.REACH_ADS,
        CREATIVES.DYNAMIC_ADS,
      ],
      allowedInDevices:    [DEVICES.ALL, DEVICES.MOBILE, DEVICES.DESKTOP],
      allowedIn:           [
        restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
        restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
        restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
        restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
      ],
      info:                [
        {
          title:   'Placements',
          content: `<p>Messenger-Home and Sponsored Messages</p>`
        },
        {
          title:   'Devices',
          content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
        }
      ],
      children:            [
        {
          key:                 KEYS.MESSENGER_HOME,
          value:               'Home',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.MOBILE],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL)
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Messenger-Home-Messenger Home</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.MOBILE}"></ai-placement-devices>`
            }
          ],
        },
        {
          key:                 KEYS.SPONSORED_MESSAGES,
          value:               'Sponsored Messages',
          allowedInObjectives: [
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.APP_INSTALLS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.CONVERSIONS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.LINK_CLICKS,
            OBJECTIVES.REACH,
            OBJECTIVES.PRODUCT_CATALOG_SALES,
          ],
          allowedInCreatives:  [
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO,
            CREATIVES.APP_ADS_DESKTOP,
            CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE,
            CREATIVES.REACH_ADS,
            CREATIVES.DYNAMIC_ADS,
          ],
          allowedInDevices:    [DEVICES.ALL, DEVICES.MOBILE, DEVICES.DESKTOP],
          allowedIn:           [
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.APP_INSTALLS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.CONVERSIONS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO, DEVICES.ALL),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_DESKTOP, DEVICES.DESKTOP),
            restrictionKey(OBJECTIVES.LINK_CLICKS, CREATIVES.APP_ADS_PHOTO_VIDEO_MOBILE, DEVICES.MOBILE),
            restrictionKey(OBJECTIVES.REACH, CREATIVES.REACH_ADS, DEVICES.ALL),
            restrictionKey(OBJECTIVES.PRODUCT_CATALOG_SALES, CREATIVES.DYNAMIC_ADS, DEVICES.ALL),
          ],
          info:                [
            {
              title:   'Placements',
              content: `<p>Messenger-Sponsored Messages-Sponsored Messages</p>`
            },
            {
              title:   'Devices',
              content: `<ai-placement-devices data-devices="${DEVICES.ALL}"></ai-placement-devices>`
            }
          ]
        }
      ]
    }
  ];
}
