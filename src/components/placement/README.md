# Placement Web Component

You can use this component in order to select facebook [targeting placement](https://developers.facebook.com/docs/marketing-api/targeting-specs/v2.9#placement).

### Tag name


```
<custom-element-demo>
  <template>
    <link rel="import" href="my-element.html">
    <link rel="import" href="../other-element/other-element.html">
    <next-code-block></next-code-block>
  </template>
</custom-element-demo>
```

### Styling

`<paper-button>` provides the following custom properties and mixins
for styling:

Custom property | Description | Default
----------------|-------------|----------
`--paper-button-ink-color` | Background color of the ripple | Based on the button's color
`--paper-button` | Mixin applied to the button | `{}`


| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  left-aligned | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |
