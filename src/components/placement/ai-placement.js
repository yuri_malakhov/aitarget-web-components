import './ai-placement-info';
import './ai-placement-devices';
import tpl from './ai-placement.tpl.html';
import styles from './ai-placement.style.html';
import { placements } from './placements';
import { CREATIVES, DEVICES, OBJECTIVES } from './constants';

class AiPlacement extends HTMLElement {
  constructor () {
    super();
    this._root = this.attachShadow({ mode: 'open' });

    // Data
    this._value = Object.assign({}, AiPlacement.getDefaultValue());

    this.placementData = placements();

    this._objective = this.getAttribute('objective') || OBJECTIVES.LINK_CLICKS;
    this._creative = this.getAttribute('creative') || CREATIVES.LINK_ADS_PAGE_CONNECTED_PHOTO_VIDEO;
    this._device = this.getAttribute('device') || DEVICES.ALL;

    // Main template
    const $template = document.createElement('template');
    $template.innerHTML = styles + tpl;
    this.$template = document.importNode($template.content, true);

    // Repeatable row template
    const $rowTemplate = this.$template.getElementById('ai-placement-row');
    this.$rowTemplate = document.importNode($rowTemplate.content, true);
    const $tickTemplate = this.$template.getElementById('tick');
    this.$tickTemplate = document.importNode($tickTemplate.content, true);
    const $deviceTemplate = this.$template.getElementById('device');
    this.$deviceTemplate = document.importNode($deviceTemplate.content, true);
  }

  static getDefaultValue () {
    return {
      facebook_positions:         [],
      instagram_positions:        [],
      audience_network_positions: [],
      messenger_positions:        []
    };
  }

  set objective (value) {
    if (value === null) {
      return;
    }
    this._objective = value;
    this._updateLigibility();
  }

  get objective () {
    return this._objective;
  }

  set creative (value) {
    if (value === null) {
      return;
    }
    this._creative = value;
    this._updateLigibility();
  }

  get creative () {
    return this._creative;
  }

  set device (value) {
    if (value === null) {
      return;
    }
    this._device = value;
    this._updateLigibility();
  }

  get device () {
    return this._device;
  }

  getUpdatedValue () {
    return this.placementRows
               .filter(($placementRow) => {
                 return $placementRow.dataset.selected === 'true'
                   && $placementRow.dataset.id
                   && !$placementRow.classList.contains('ineligible')
               })
               .reduce((acc, $placementRow) => {
                 // noinspection JSUnresolvedVariable
                 acc[$placementRow.dataset.groupId].push($placementRow.dataset.id);
                 return acc;
               }, Object.assign({}, AiPlacement.getDefaultValue()));
  }

  set value (value) {
    if (this._value === value) {
      return;
    }
    this._value = Object.assign({}, AiPlacement.getDefaultValue(), value);
    this._render();
    this.dispatchEvent(new Event('change'));
  }

  get placementRows () {
    const placementRows = this._root.querySelectorAll('li');
    return Array.prototype.slice.apply(placementRows);
  }

  get value () {
    return this._value;
  }

  connectedCallback () {
    this.$parentList = document.createElement('ul');
    this.$parentList.id = 'parent-list';

    this.$device = this.$deviceTemplate.cloneNode(true)
                       .querySelector('select');

    this._createDom();

    this._onTickClick();
    this._onInputChange();
    this._onDeviceChange();

    this.$template.appendChild(this.$device);
    this.$template.appendChild(this.$parentList);

    this._root.appendChild(this.$template);

    this._fillAllInfoCards(this.placementData);

    this._updateLigibility();

    const initialValue = this.getAttribute('value');
    if (initialValue !== null) {
      this._value = initialValue;
    }

    this._render();
  }

  _render () {
    this.placementRows.forEach(($placementRow) => {
      const placementGroupId = $placementRow.dataset.groupId;
      const placementId = $placementRow.dataset.id;

      // For any row, mostly children
      if (this.value[placementGroupId].indexOf(placementId) > -1) {
        $placementRow.querySelector('input').checked = true;
      }

      // If parent row
      if (placementGroupId && !placementId) {
        placements()
          .forEach((placement) => {
            if (placement.key === placementGroupId) {
              const indeterminate = this.value[placementGroupId].length > 0
                && (this.value[placementGroupId].length < placement.children.length);
              const $checkbox = $placementRow.querySelector('input');
              $checkbox.indeterminate = indeterminate;
              $checkbox.checked = !indeterminate && this.value[placementGroupId].length === placement.children.length;
              $placementRow.setAttribute('data-selected', $checkbox.checked);
            }
          });
      }

    });
  }

  static get observedAttributes () {
    return ['value', 'objective', 'creative', 'device'];
  }

  /* eslint-disable complexity */
  attributeChangedCallback (name, oldValue, newValue) {
    console.log(`name, oldValue, newValue: `, name, oldValue, newValue);
    if (oldValue !== newValue) {
      switch (name) {
        case 'value':
          if (this._touched === false) {
            this._value = newValue;
            this._render();
          }
          break;
        case 'objective':
          this.objective = newValue;
          break;
        case 'creative':
          this.creative = newValue;
          break;
        case 'device':
          this.device = newValue || DEVICES.ALL;
          break;
        default:
          break;
      }
    }
  }

  /* eslint-enable complexity */

  /**
   * Create element DOM structure
   * @private
   */
  _createDom () {
    this.placementData
        .forEach((placementGroup) => {
          const $placementGroup = this.$rowTemplate.cloneNode(true);
          const $placementGroupTick = this.$tickTemplate.cloneNode(true);
          const $placementGroupName = $placementGroup.getElementById('name');
          const $placementGroupItem = $placementGroup.querySelector('li');

          // Open by default
          /*$placementGroupItem.classList.add('open');
          $tick.querySelector('ai-icon-tick')
               .setAttribute('data-mod', 'open');*/

          $placementGroupName.innerText = placementGroup.value;
          $placementGroupItem.setAttribute('data-group-id', placementGroup.key);
          $placementGroupItem.classList.add('parent-item');
          $placementGroupItem.prepend($placementGroupTick);

          if (placementGroup.children) {
            const $childrenList = document.createElement('ul');
            $childrenList.classList.add('children');
            placementGroup.children.forEach((placement) => {
              const $placement = this.$rowTemplate.cloneNode(true);
              const $name = $placement.getElementById('name');
              const $placementRow = $placement.querySelector('li');

              $name.innerText = placement.value;
              $placementRow.setAttribute('data-id', placement.key);
              $placementRow.setAttribute('data-group-id', placementGroup.key);
              $placementRow.setAttribute('data-selected', String(this.value[placementGroup.key].indexOf(placement.key) > -1));
              $placementRow.classList.add('child-item');

              $childrenList.appendChild($placement);
              $placementGroup.appendChild($childrenList);
            });
          }

          this.$parentList.appendChild($placementGroup);
        });
  }

  /**
   * Handle placement row change event
   * @private
   */
  _onInputChange () {
    this.$parentList.querySelectorAll('input')
        .forEach(($input) => {
          $input.addEventListener('change', (e) => {
            const $placementRow = e.path[2];
            const $checkbox = e.path[0];
            const placementGroupId = $placementRow.dataset.groupId;
            const placementId = $placementRow.dataset.id;

            $checkbox.indeterminate = false;
            $placementRow.setAttribute('data-selected', $checkbox.checked);

            // If parent row
            if (placementGroupId && !placementId) {
              const $childrenPlacementRows = this._root.querySelectorAll(`[data-id][data-group-id=${placementGroupId}]`);
              $childrenPlacementRows.forEach(($childPlacementRow) => {
                $childPlacementRow.querySelector('input').checked = $checkbox.checked;
                $childPlacementRow.setAttribute('data-selected', $checkbox.checked);
              });
            }
            this.value = this.getUpdatedValue();
          });
        });
  }

  /**
   * Handle tick click for toggling group row
   * @private
   */
  _onTickClick () {
    this.$parentList.querySelectorAll('.tick')
        .forEach(($tick) => {
          $tick.addEventListener('click', () => {
            const $groupItem = $tick.parentNode;
            $groupItem.classList.toggle('open');
            const iconTickMod = $groupItem.classList.contains('open') ? 'open' : 'closed';
            $tick.querySelector('ai-icon-tick')
                 .setAttribute('data-mod', iconTickMod)
          });
        });
  }

  /**
   * Set placements checkboxes and info blocks to ineligible if placement is not
   * supported for curently selected objective, creative, device.
   * @private
   */
  _updateLigibility () {
    this.placementRows
        /* eslint-disable complexity */
        .forEach(($placementRow) => {
          const groupId = $placementRow.dataset.groupId;
          const id = $placementRow.dataset.id;
          const placementGroup = this.placementData.find((_) => _.key === groupId);

          const placement = placementGroup.children.find((_) => _.key === id);

          const _data = id ? placement : placementGroup;

          const allowedInObjectives = this.objective ? _data.allowedInObjectives.indexOf(this.objective) > -1 : true;
          const allowedInCreatives = this.creative ? _data.allowedInCreatives.indexOf(this.creative) > -1 : true;
          // TODO: update device select after checking all available placements
          const allowedInDevices = this.device === DEVICES.ALL || (this.device ? _data.allowedInDevices.indexOf(this.device) > -1 : true);

          const ineligible = !allowedInObjectives || !allowedInCreatives || !allowedInDevices;

          $placementRow.classList.toggle('ineligible', ineligible);

          const $checkbox = $placementRow.querySelector('input');
          $checkbox.checked = !ineligible;

          $placementRow.setAttribute('data-selected', $checkbox.checked);
        });
    /* eslint-enable complexity */

    this.value = this.getUpdatedValue();
  }

  /**
   * Enable filtering by device
   * @private
   */
  _onDeviceChange () {
    this.$device.addEventListener('change', () => {
      this.device = this.$device.value;
    });
  }

  /**
   * Fill in all rows info cards with data
   * @param placementData
   * @private
   */
  _fillAllInfoCards (placementData) {
    this.placementRows.forEach(($placementRow) => {
      const $infoCard = $placementRow.querySelector('ai-card');

      const groupId = $placementRow.dataset.groupId;
      const id = $placementRow.dataset.id;

      const placementGroup = placementData.find((_) => _.key === groupId);
      const placement = placementGroup.children.find((_) => _.key === id);

      if (id) {
        this._fillOneInfoCard($infoCard, placement.info);
      } else {
        this._fillOneInfoCard($infoCard, placementGroup.info);
      }
    });
  }

  /**
   * Fill in info card with data
   * @param $infoCard
   * @param data
   * @private
   */
  _fillOneInfoCard ($infoCard, data) {
    if (!data || !data.length) {
      $infoCard.classList.add('empty');
      return;
    }
    $infoCard.innerHTML = '';
    data.forEach((info) => {
      $infoCard.innerHTML += `<ai-placement-info>
                                <h3 slot='title'>${info.title}</h3>
                                <div slot='content'>${info.content}</div>
                              </ai-placement-info>
                              <hr>`;
    });
  }
}

window.customElements.define('ai-placement', AiPlacement);
