class AiSelect extends HTMLElement {
  constructor () {
    super();
    // Define shadow DOM
    this._root = this.attachShadow({ mode: "open" });

    // Main template
    const $template = document.createElement("template");
    $template.innerHTML = `<select>
                            <slot></slot>
                           </select>`;
    this.$template = document.importNode($template.content, true);
  }

  connectedCallback () {
    this._root.appendChild(this.$template);
  }
}

window.customElements.define('ai-select', AiSelect);
