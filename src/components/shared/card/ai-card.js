class AiCard extends HTMLElement {
  constructor () {
    super();
    // Define shadow DOM
    this._root = this.attachShadow({ mode: "open" });

    // Main template
    const $template = document.createElement("template");
    $template.innerHTML = `<slot></slot>
                           <style>
                             :host{
                               display: block;
                               border: 1px solid #dadcde;
                               background-color: #ffffff;
                               box-shadow: 2px 2px 3px #dadcde;
                             }
                           </style>`;
    this.$template = document.importNode($template.content, true);
  }

  connectedCallback () {
    this._root.appendChild(this.$template);
  }
}

window.customElements.define('ai-card', AiCard);
