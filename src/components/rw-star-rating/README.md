## Human Readable Name Of You Component

**Description**. Answer questions **why this component is needed** and **what problem does it solve**. If necessary, *attach links* to other resources, examples, and documentations.

---

### Installation

**Local:**
```
npm install <project-name>

```

**Remote**
```
<script src="https://https://apps.aitarget.com/aitarget-web-components/dist/es5/fba-component-name.js"></script>
```

### Polyfills

Please refer to the documentations of [webcomponents/polyfills](https://github.com/WebComponents/webcomponentsjs) as well as related projects such as [shadydom](https://github.com/webcomponents/shadydom) and [shadycss](https://github.com/webcomponents/shadycss).

The easiest way is to installs polyfills is from [cdnjs.com](https://cdnjs.com/libraries/webcomponentsjs):
```
<!-- Load Custom Elements es5 adapter -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/1.0.4/custom-elements-es5-adapter.js"></script>
<!-- Load polyfills; note that "loader" will load these async -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/1.0.4/webcomponents-loader.js"></script>
<!-- Load the es5 compiled custom element definition -->
<script src="https://https://apps.aitarget.com/aitarget-web-components/dist/es5/fba-component-name.js"></script>

<!-- Use the custom element -->
<fba-component-name></fba-component-name>
```

### Usage

You can use the component in any place on a page like so:
```
<fba-component-name></fba-component-name>
```

### Demo

Insert image, gif, video, codepen or fiddle widgets, etc.

### Attributes


### Properties

### Methods

| Name    | Arguments  | Return | Description            |
|:--------|:----------:|-------:|:-----------------------|
| method1 |            |      - | Some desc if necessary |
| method2 | arg1, arg2 | number | No desc                |
| method3 |   {...}    |     [] | –                      |

### Events

### Styling

`<fba-component-name>` provides the following custom properties and mixins
for styling:

| Custom property                 | Description                       | Default   |
|:--------------------------------|:----------------------------------|:----------|
| `--fba-component-name-bg-color` | Background color of the component | `#ffffff` |
| `--fba-component-namen`         | Mixin applied to the button       | `{}`      |
