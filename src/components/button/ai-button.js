import button from './ai-button.button.tpl.html';
import link from './ai-button.link.tpl.html';
import styles from './ai-button.styles.html';

class AiButton extends HTMLElement {
  constructor () {
    super();
    this._root = this.attachShadow({ mode: 'open' });
    this._disabled = false;
  }

  connectedCallback () {
    this._type = this.getAttribute('data-type') || 'button';
    this._mode = this.getAttribute('data-mode') || 'default';
    this._size = this.getAttribute('data-size') || 'md';

    const $template = document.createElement('template');
    const templates = { button, link };
    $template.innerHTML = styles + templates[this._type];

    this._$template = document.importNode($template.content, true);
    this._$content = this._$template.querySelector('.content');
    this._$button = (this._type === 'button') ?
      this._$template.querySelector('button')
      : this._$template.querySelector('slot');

    this._root.appendChild(this._$template);

    this._render();
    this._$button.addEventListener('click', (event) => {
      if (this._disabled) {
        event.preventDefault();
        event.stopPropagation();
      }
    });
  }

  _render () {
    if (this._disabled) {
      this._$button.setAttribute('disabled', '');
    }
    console.log(`this: `, this);
    // this._$button.classList.add(this._mode, this._size);
    this._$content.classList.add(this._mode, this._size);
  }

  static get observedAttributes () {
    return ['disabled'];
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if (oldValue !== newValue) {
      switch (name) {
        case 'disabled':
          this._disabled = (newValue !== null);
          // this._render();
          break;
        default:
          break;
      }
    }
  }
}

window.customElements.define('ai-button', AiButton);
