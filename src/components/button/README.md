<a href="/"><< Collection of web components</a>

## Web Component button



### Usage

You can use the component in any place on a page like so:
```
<ai-button></ai-button>
```

### Demo

<p data-height="265" data-theme-id="light" data-slug-hash="4a697dc484c9e5aafa4469817bb36cc8" data-default-tab="html,result" data-user="Aitarget" data-embed-version="2" data-pen-title="4a697dc484c9e5aafa4469817bb36cc8" data-editable="true" class="codepen">See the Pen <a href="https://codepen.io/Aitarget/pen/4a697dc484c9e5aafa4469817bb36cc8/">4a697dc484c9e5aafa4469817bb36cc8</a> by Aitarget (<a href="https://codepen.io/Aitarget">@Aitarget</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>
### Attributes
| Name      |            Arguments             | Default |
|:----------|:--------------------------------:|-------:|
| data-mode | default, info, success, warning, danger  | html button |
| data-type |           button, link           | button  |
| data-size |lg, md, sm, xs| md|
| disabled  |                -                 |  -      |
