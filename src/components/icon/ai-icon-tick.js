class AiIconTick extends HTMLElement {
  constructor () {
    super();
    // Define shadow DOM
    this._root = this.attachShadow({ mode: "open" });

    // Main template
    const $template = document.createElement("template");
    $template.innerHTML = `<div></div>
                          <style>
                          :host {
                            display: inline-block;
                            text-align: center;
                            vertical-align: middle;
                            width: 14px;
                          }
                          :host div:before {
                            content: "";
                            position: relative;
                            left: 2.5px;
                            display: inline-block;
                            border: 5px solid transparent;
                            border-left-color: #aaaaaa;
                          }
                          
                          :host([data-mod=open]) div:before {
                            content: "";
                            position: relative;
                            top: 2.5px;
                            left: 0;
                            display: inline-block;
                            border: 5px solid transparent;
                            border-top-color: #aaaaaa;
                          }
                          </style>`;
    this.$template = document.importNode($template.content, true);
  }

  connectedCallback () {
    this._root.appendChild(this.$template);
  }
}

window.customElements.define('ai-icon-tick', AiIconTick);
