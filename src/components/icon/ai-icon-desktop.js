class AiIconDesktop extends HTMLElement {
  constructor () {
    super();
    // Define shadow DOM
    this._root = this.attachShadow({ mode: "open" });

    // Main template
    const $template = document.createElement("template");
    $template.innerHTML = `<svg fill="#666666" height="14" viewBox="0 0 24 24" width="14" xmlns="http://www.w3.org/2000/svg">
                             <path d="M0 0h24v24H0z" fill="none"/>
                             <path d="M21 2H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h7l-2 3v1h8v-1l-2-3h7c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 12H3V4h18v10z"/>
                           </svg>
                           <style>:host{display: inline-block; vertical-align: middle}</style>`;
    this.$template = document.importNode($template.content, true);
  }

  connectedCallback () {
    this._root.appendChild(this.$template);
  }
}

window.customElements.define('ai-icon-desktop', AiIconDesktop);
