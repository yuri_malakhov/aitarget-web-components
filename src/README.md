## A collection of basic low-level UI web components.

**Description**. Answer questions **why this component is needed** and **what problem does it solve**. If necessary, *attach links* to other resources, examples, and documentations.

---

### Button

Very beautiful [button](components/button/)

### Placement

Very beautiful [placement](components/placement/)

### Star Rating

Very beautiful [star rating](components/rw-star-rating/)
