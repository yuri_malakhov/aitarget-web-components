import '@webcomponents/webcomponentsjs/webcomponents-lite';

import './components/rw-star-rating/rw-star-rating';
import './components/placement/ai-placement';
import './components/button/ai-button';
import './components/icon/ai-icon';
import './components/shared';
