const { resolve, join } = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = env => {
  return {
    entry:     {
      'aitarget-web-components': './aitarget-web-components.js',
    },
    output:    {
      filename: '[name].js',
      path:     resolve(__dirname, 'dist'),
      pathinfo: true
    },
    devServer: {
      contentBase: join(__dirname, "dist"),
      compress:    true,
      host:        "lo.aitarget.com",
      https:       {
        key:  fs.readFileSync("server.key"),
        cert: fs.readFileSync("server.crt")
      },
      port:        8080
    },
    context:   resolve(__dirname, 'src'),
    devtool:   env.prod ? 'source-map' : 'cheap-module-eval-source-map',
    module:    {
      loaders: [
        {
          test:    /\.js$/,
          loader:  'babel-loader!eslint-loader',
          exclude: /node_modules/
        },
        {
          test: /\.html$/,
          use:  ["html-loader"],
          exclude: /index.html/
        }
      ],
    },
    plugins:
               [
                 new webpack.IgnorePlugin(/vertx/),
                 new webpack.optimize.ModuleConcatenationPlugin(),
                 new HtmlWebpackPlugin({
                   template: 'index.html',
                   minify: false
                 }),
                 new CopyWebpackPlugin([
                   {
                     from: '../node_modules/bootmark/dist/bootmark.bundle.min.js'
                   },
                   {
                     from: './**/*.md',
                   },
                   {
                     from:      './components/**/index.html',
                     transform: function (content) {
                       return `<!DOCTYPE html>
                       <html lang="en">
                       <head>
                         <meta charset="UTF-8">
                         <title>A collection of basic low-level UI Vanilla Web Components</title>
                         <base href="/">
                         <link href="//res.cloudinary.com/dbd0yzqab/image/upload/v1487080491/aitarget-components/favicon.png"
                               rel="shortcut icon"
                               type="image/x-icon">
                         <style>*:not(:defined) { visibility: hidden; }</style>
                         <script src="bootmark.bundle.min.js"></script>
                       </head>
                       <body>
                         ${content}
                         <script src="aitarget-web-components.js"></script>
                       </body>`;
                     }
                   }
                 ], { copyUnmodified: true })
               ]
  }
};
